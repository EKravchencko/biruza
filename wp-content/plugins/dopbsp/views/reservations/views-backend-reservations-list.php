<?php

/*
* Title                   : Booking System PRO (WordPress Plugin)
* Version                 : 2.0
* File                    : views/reservations/views-backend-reservations-list.php
* File Version            : 1.0.2
* Created / Last Modified : 13 October 2014
* Author                  : Dot on Paper
* Copyright               : © 2012 Dot on Paper
* Website                 : http://www.dotonpaper.net
* Description             : Booking System PRO back end reservations list views class.
*/

    if (!class_exists('DOPBSPViewsBackEndReservationsList')){
        class DOPBSPViewsBackEndReservationsList extends DOPBSPViewsBackEndReservations{
            /*
             * Constructor
             */
            function DOPBSPViewsBackEndReservationsList(){
            }
            
            /*
             * Returns reservations template.
             * 
             * @param args (array): function arguments
             *                      * reservations (array): reservations list
             * 
             * @return reservations HTML page
             */
            function template($args = array()){
                global $DOPBSP;
                
                $reservations = $args['reservations'];
?>
                <ul class="dopbsp-reservations-list">
<?php
                /*
                 * Check if reservations exist.
                 */
                if (count($reservations) > 0){
                    foreach ($reservations as $reservation){
                        $DOPBSP->views->backend_reservation->template(array('reservation' => $reservation));
                    }
                }
                else{
?>                    
                    <li class="dopbsp-no-data"><?php echo $DOPBSP->text('RESERVATIONS_NO_RESERVATIONS'); ?></li>
<?php                    
                }
?>
                </ul>
				
				<script>
					jQuery('.editer_jk_cust a').on('click', function() {
						jQuery('.editer_jk_cust a').hide();								
						var editorId = jQuery(this).parent().attr('editor-id');
						saveButton = "<div class=\"saveMePlease\" editor-id='" + editorId + "' style=\"background: url('../wp-content/plugins/dopbsp/assets/gui/images/save.png'); float: left; height: 16px; width: 16px; margin: 6px; cursor: pointer;\"></div>"
						var reservWrap = jQuery(this).parent().parent();
						var editableField = reservWrap.find(".editable-price .dopbsp-data-value")
						var oldPriceWithText = editableField.text();
						var oldPrice = parseFloat(oldPriceWithText);
						var currency = "<span class='currency_jk'>" + oldPriceWithText.replace(oldPrice.toString(),"") + "</span>";
						editableField.text("");
						editableField.append(saveButton + "<input style=\"width: 100px;\" type=\"text\" value=\"" + oldPrice + "\"></input>" + currency);								
					});
					jQuery('.saveMePlease').live('click', function () {	
						var editorId = jQuery(this).attr('editor-id');
						var saveMePlease = jQuery(this);
						jQuery.ajax({
							url: '../wp-content/plugins/dopbsp/assets/ajax/savePrice.php',
							type: "POST",
							data: {
								newPrice : jQuery(this).next("input[type='text']").val(),
								editor_Id : editorId, 
							},
							success: function(data) {
								if(data == "error") {
									alert("Задано неверное число!\nЗначение не может быть отрицательным.\nДля задания дробных числе используйте точку.");
								} else {
									jQuery('.editer_jk_cust a').show();
									var vurency = saveMePlease.next().next('.currency_jk').text();
									var deng = saveMePlease.next("input[type='text']").val();
									saveMePlease.parent().html(deng + ' ' + vurency);
								}
							}
						});
					});
				</script>
<?php
            }
        }
    }