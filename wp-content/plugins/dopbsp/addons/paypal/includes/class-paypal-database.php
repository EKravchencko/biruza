<?php

/*
* Title                   : Booking System Pro (WordPress Plugin)
* Version                 : 2.0
* File                    : addons/paypal/includes/class-paypal-database.php
* File Version            : 1.0.2
* Created / Last Modified : 28 December 2014
* Author                  : Dot on Paper
* Copyright               : © 2012 Dot on Paper
* Website                 : http://www.dotonpaper.net
* Description             : Booking System PRO PayPal database PHP class. IMPORTANT! Version, configuration, initialization, initial data, update, need to be in same file because of issues with instalation/update via FTP.
*/

    if (!class_exists('DOPBSPPayPalDatabase')){
        class DOPBSPPayPalDatabase{
            /*
             * Private variables.
             */
            private $db_version = 1.0;
            
            /*
             * Constructor
             */
            function DOPBSPPayPalDatabase(){
                add_filter('dopbsp_filter_database_configuration', array(&$this, 'config'));
                
                add_action('init', array(&$this, 'init'), 11);
                 
                /*
                 * Change database version if requested.
                 */
                if (DOPBSP_CONFIG_INIT_DATABASE){
                    update_option('DOPBSP_db_version_paypal', '');
                }
            }
            
            /*
             * Initialize plugin tables.
             */
            function init(){
                global $DOPBSP;
                
                $db_config = $DOPBSP->classes->database->db_config;
                $db_collation = $DOPBSP->classes->database->db_collation;
                
                /*
                 * Get current database version.
                 */
                $current_db_version = get_option('DOPBSP_db_version_paypal');
                 
                if ($this->db_version != $current_db_version){
                    require_once(str_replace('\\', '/', ABSPATH).'wp-admin/includes/upgrade.php');
                    
                    $sql_settings_notifications = "CREATE TABLE ".$DOPBSP->tables->settings_notifications." (
                                            id BIGINT UNSIGNED NOT NULL,
                                            calendar_id BIGINT UNSIGNED DEFAULT ".$db_config->settings_notifications['calendar_id']." NOT NULL,
                                            send_paypal_admin VARCHAR(6) DEFAULT '".$db_config->settings_notifications['send_paypal_admin']."' COLLATE ".$db_collation." NOT NULL,
                                            send_paypal_user VARCHAR(6) DEFAULT '".$db_config->settings_notifications['send_paypal_user']."' COLLATE ".$db_collation." NOT NULL,
                                            UNIQUE KEY id (id),
                                            KEY calendar_id (calendar_id)
                                        );";
                    dbDelta($sql_settings_notifications);
                    
                    $sql_settings_payment = "CREATE TABLE ".$DOPBSP->tables->settings_payment." (
                                            id BIGINT UNSIGNED NOT NULL,
                                            calendar_id BIGINT UNSIGNED DEFAULT ".$db_config->settings_payment['calendar_id']." NOT NULL,
                                            paypal_enabled VARCHAR(6) DEFAULT '".$db_config->settings_payment['paypal_enabled']."' COLLATE ".$db_collation." NOT NULL,
                                            paypal_credit_card VARCHAR(6) DEFAULT '".$db_config->settings_payment['paypal_credit_card']."' COLLATE ".$db_collation." NOT NULL,
                                            paypal_username VARCHAR(128) DEFAULT '".$db_config->settings_payment['paypal_username']."' COLLATE ".$db_collation." NOT NULL,
                                            paypal_password VARCHAR(128) DEFAULT '".$db_config->settings_payment['paypal_password']."' COLLATE ".$db_collation." NOT NULL,
                                            paypal_signature VARCHAR(128) DEFAULT '".$db_config->settings_payment['paypal_signature']."' COLLATE ".$db_collation." NOT NULL,
                                            paypal_sandbox_enabled VARCHAR(6) DEFAULT '".$db_config->settings_payment['paypal_sandbox_enabled']."' COLLATE ".$db_collation." NOT NULL,
                                            paypal_sandbox_username VARCHAR(128) DEFAULT '".$db_config->settings_payment['paypal_sandbox_username']."' COLLATE ".$db_collation." NOT NULL,
                                            paypal_sandbox_password VARCHAR(128) DEFAULT '".$db_config->settings_payment['paypal_sandbox_password']."' COLLATE ".$db_collation." NOT NULL,
                                            paypal_sandbox_signature VARCHAR(128) DEFAULT '".$db_config->settings_payment['paypal_sandbox_signature']."' COLLATE ".$db_collation." NOT NULL,
                                            paypal_redirect VARCHAR(128) DEFAULT '".$db_config->settings_payment['paypal_redirect']."' COLLATE ".$db_collation." NOT NULL,
                                            UNIQUE KEY id (id),
                                            KEY calendar_id (calendar_id)
                                        );";
                    dbDelta($sql_settings_payment);

                    /*
                     * Update database version.
                     */
                    if ($current_db_version == ''){
                        add_option('DOPBSP_db_version_paypal', $this->db_version);
                    }
                    else{
                        update_option('DOPBSP_db_version_paypal', $this->db_version);
                    }
                }
            }
            
            /*
             * Set payment database configuration for PayPal.
             * 
             * @param $db_config (object): current database configuration
             * 
             * @return updated database configuration
             */
            function config($db_config){
                /*
                 * Settings notifications.
                 */
                $db_config->settings_notifications['send_paypal_admin'] = 'true';
                $db_config->settings_notifications['send_paypal_user'] = 'true';
                
                /*
                 * Settings payment.
                 */
                $db_config->settings_payment['paypal_enabled'] = 'false';
                $db_config->settings_payment['paypal_credit_card'] = 'false';
                $db_config->settings_payment['paypal_username'] = '';
                $db_config->settings_payment['paypal_password'] = '';
                $db_config->settings_payment['paypal_signature'] = '';
                $db_config->settings_payment['paypal_sandbox_enabled'] = 'false';
                $db_config->settings_payment['paypal_sandbox_username'] = '';
                $db_config->settings_payment['paypal_sandbox_password'] = '';
                $db_config->settings_payment['paypal_sandbox_signature'] = '';
                $db_config->settings_payment['paypal_redirect'] = '';
                
                return $db_config;
            }
        }
    }