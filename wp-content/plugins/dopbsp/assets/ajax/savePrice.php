<?
	require_once("../../../../../wp-config.php");
	require_once("../../../../../wp-load.php");
	require_once("../../../../../wp-includes/wp-db.php");

	global $wpdb;
	if(!empty($_POST['editor_Id']) && !empty($_POST["newPrice"]) && is_numeric($_POST["newPrice"]) && $_POST["newPrice"] > 0) {
		$wpdb->update(
			"wp_dopbsp_reservations",
			Array("price_total" => $_POST['newPrice']),
			Array("id" => $_POST['editor_Id']),
			Array("%f"),
			Array("%d")
		);
		echo ("success");
	} else {
		echo ("error");
	}

?>