<?php
/**
 * The Header for our theme.
 *
 * @package Skeleton WordPress Theme Framework
 * @subpackage skeleton
 * @author Simple Themes - www.simplethemes.com
 */
?>
<!doctype html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes();?>> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes();?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes();?>> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" <?php language_attributes();?>> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!--><html <?php language_attributes();?>> <!--<![endif]-->

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<title><?php wp_title( '|', true, 'right' ); ?></title>

<?php 
	/* 
	<link rel="profile" href="http://gmpg.org/xfn/11"> 
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	*/ 
?> 

<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
<![endif]-->


<!-- Mobile Specific Metas
================================================== -->

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


<?php wp_head(); ?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom_onvolga.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/jquery/css/ui-lightness/jquery-ui-1.9.2.custom.min.css">
<script src="<?php echo get_template_directory_uri(); ?>/jquery/js/jquery-1.8.3.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/jquery/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/jquery/js/rus-datapicker.js"></script>
<script>
$(function() {	
	$("#foogall-preview-image-jk a br, .DOPBSPSearch-sort > p br, .dopbsp-module.dopbsp-price br").remove();
	$.datepicker.setDefaults($.extend($.datepicker.regional["ru"]));
	var today = new Date();	var month = today.getMonth() + 1;
	$( "#div_date_in input" ).datepicker({
		minDate : today.getDate() + '.' + month + '.' + today.getFullYear(),
		onClose: function( selectedDate ) {
			$( "#div_date_out input" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#div_date_out input" ).datepicker( {
		minDate : today.getDate() + '.' + month + '.' + today.getFullYear(),
		onClose: function( selectedDate ) {
			$( "#div_date_in input" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	$(".wrapper_foogool_jk .owl-item .foo-item").live("click", function () {
		$("#foogall-preview-image-jk a").attr("href", $(this).attr("data-full-src"));
		$("#foogall-preview-image-jk img").attr("src", $(this).attr("data-full-src"));
	});
	
	<?php if(!empty($_GET["chekin"]) || !empty($_GET["chekout"])): ?>
		function toStringDate(date) {
			var date_ar = date.split('-');
			var arr_m = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
			return date_ar[2] + ' ' + arr_m[parseInt(date_ar[1], 10) - 1] + ' ' + date_ar[0];
		}			
	<?php endif; ?>
	<?php if(!empty($_GET["chekin"])) : ?>
		var chekin_from_slider = $("#chekin_from_slider").val().split('.').reverse().join('-');
		$("#DOPBSPSearch-check-in1").val(chekin_from_slider);
		$("#div_date_in input").val($("#chekin_from_slider").val());
		setInterval( function () { 
			if(!$(".DOPBSPSearch-check-in-view.hasDatepicker").is(":focus") && $("#DOPBSPSearch-check-in1").val() != "") {
				$(".DOPBSPSearch-check-in-view.hasDatepicker").val(toStringDate($("#DOPBSPSearch-check-in1").val()))
			}
		}, 500);
	<?php endif; ?>
	<?php if(!empty($_GET["chekout"])) : ?>
		var chekout_from_slider = $("#chekout_from_slider").val().split('.').reverse().join('-');
		$("#DOPBSPSearch-check-out1").val(chekout_from_slider);
		$("#div_date_out input").val($("#chekout_from_slider").val());
		setInterval( function () { 
			if(!$(".DOPBSPSearch-check-out-view.hasDatepicker").is(":focus") && $("#DOPBSPSearch-check-out1").val() != "") {
				$(".DOPBSPSearch-check-out-view.hasDatepicker").val(toStringDate($("#DOPBSPSearch-check-out1").val()))
			}
		}, 500);
	<?php endif; ?>	
	
	function resizeSliderDatapicker() {
		if($("#after-menu-widget > div:nth-child(2)").height() > 213) {
			$("#after-menu-widget > div:first-child").css("height", $("#after-menu-widget > div:nth-child(2)").height());
		}		
	}
	
	setInterval(resizeSliderDatapicker, 500);
});
</script>

</head>
<body <?php body_class(); ?>>

<?php if(!empty($_GET["chekin"])) : ?>
	<input id="chekin_from_slider" type="hidden" value="<?php echo $_GET["chekin"]; ?>">
<?php endif; ?>
<?php if(!empty($_GET["chekout"])) : ?>
	<input id="chekout_from_slider" type="hidden" value="<?php echo $_GET["chekout"]; ?>">
<?php endif; ?>

<?php
	do_action('skeleton_header');
	do_action('skeleton_navbar');
?>